
package data;

import java.util.Iterator;

import pojo.*;

/**
 *
 * @author LHK
 */
public class DataOperator {

    /**
     *
     * @param type
     * @param id
     * @param pwd
     * @return
     */
    public boolean vaildUser(String type, String id, String pwd) {
		if (type.equals("Patient")) {
			for (Patient patient : ManageData.patientList) {
				if (patient.getId().equals(id) && patient.getPwd().equals(pwd))
					return true;
			}
			return false;

		} else if (type.equals("Doctor")) {
			for (Doctor doctor : ManageData.doctorList) {
				if (doctor.getId().equals(id) && doctor.getPwd().equals(pwd))
					return true;
			}
			return false;

		} else if (type.equals("Administrator")) {
			for (Administrator administrator : ManageData.administratorList) {
				if (administrator.getId().equals(id) && administrator.getPwd().equals(pwd))
					return true;
}
			return false;
		} else if (type.equals("Secretary")) {
			for (Secretary secretary : ManageData.secretaryList) {
				if (secretary.getId().equals(id) && secretary.getPwd().equals(pwd))
					return true;
			}
			return false;
		}
		return false;
	}

    /**
     *
     * @param patientID
     * @param patientPwd
     */
    public void removePatient(String patientID,String patientPwd) {
		Iterator<Patient> iterable = ManageData.patientList.iterator();
		while(iterable.hasNext()) {
			Patient patient = iterable.next();
			if(patientID.equals(patient.getId())) {
				iterable.remove();
			}
		}
	}

    /**
     *
     * @param userID
     * @param userPwd
     * @param userType
     */
    public void removeUser(String userID,String userPwd,String userType) {
		if("Patient".equals(userType)) {
			removePatient(userID, userPwd);
}else if("Doctor".equals(userType)) {
			Iterator<Doctor> iterable = ManageData.doctorList.iterator();
			while(iterable.hasNext()) {
				Doctor doctor = iterable.next();
				if(userID.equals(doctor.getId())) {
					iterable.remove();
				}
			}
			
		}else if("Administrator".equals(userType)) {
			Iterator<Administrator> iterable = ManageData.administratorList.iterator();
			while(iterable.hasNext()) {
				Administrator administrator = iterable.next();
				if(userID.equals(administrator.getId())) {
					iterable.remove();
				}
			}
			
		}else if("Secretary".equals(userType)){
			Iterator<Secretary> iterable = ManageData.secretaryList.iterator();
			while(iterable.hasNext()) {
				Secretary secretary = iterable.next();
				if(userID.equals(secretary.getId())) {
					iterable.remove();
				}
		}
		}
	}
}
