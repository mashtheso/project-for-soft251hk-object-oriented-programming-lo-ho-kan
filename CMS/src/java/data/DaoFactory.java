package data;

/**
 *
 * @author LHK
 */
public class DaoFactory {
	private volatile static DataOperator dataOperator;

	private DaoFactory() {
	}

    /**
     *
     * @return
     */
    public static DataOperator getSingleDataOperator() {
		if (dataOperator == null) {
			synchronized (DaoFactory.class) {
				if (dataOperator == null) {
					dataOperator = new DataOperator();
				}
			}
		}
		return dataOperator;
	}
}
