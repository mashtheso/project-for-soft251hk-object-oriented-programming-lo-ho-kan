
package data;

import java.util.LinkedList;
import java.util.List;


import pojo.Administrator;
import pojo.Appointment;
import pojo.Doctor;
import pojo.Patient;
import pojo.Prescription;
import pojo.Secretary;

/**
 *
 * @author LHK
 */
public class ManageData {

    /**
     *
     */
    public static List<Patient> patientList = new LinkedList<>();

    /**
     *
     */
    public static List<Doctor> doctorList = new LinkedList<>();

    /**
     *
     */
    public static List<Secretary> secretaryList = new LinkedList<>();

    /**
     *
     */
    public static List<Administrator> administratorList = new LinkedList<>();

    /**
     *
     */
    public static List<Appointment> appointmentsList = new LinkedList<>();

    /**
     *
     */
    public static List<Prescription> prescriptionsList = new LinkedList<>();

    /**
     *
     */
    public void init() {
		
		Patient john = new Patient("P001","Eva");
		john.setAddress("RM1,1F,ABC building, Mong Kok, Hong Kong");
		john.setGender("Male");
		john.setAge(45);
		john.setHistory("No Sick History");
                Patient frank = new Patient("P002", "Anna");
		Patient wade = new Patient("P003", "Joe");
		
		Doctor james = new Doctor("D001", "Ken");
		james.setAddress("RM4,11F,Macon building, Lai Chi Kok, Hong Kong");
		Appointment appointment = new Appointment("P1001","D1001","2019-1-1","2019-1-2");
		
	        Doctor kobe = new Doctor("D002","Anson");
		Doctor curry = new Doctor("D003","Paul");
                
		Secretary secretary = new Secretary("S001", "Amy");
                
		String medicine = "Paracetamol";
		int quantity = 10;
		String dosage = "4 per day – Every 4 hours each ";
                
		Prescription prescription = new Prescription(medicine, quantity, dosage);
		prescription.setDoctorID("D1001");
		prescription.setPatientID("P1001");
		john.setPrescription(prescription);
		john.setAppointment(appointment);
		james.setAppointment(appointment);
		
		Administrator admin = new Administrator("A001", "admin");
		
		
		ManageData.patientList.add(john);
		ManageData.patientList.add(frank);
		ManageData.patientList.add(wade);
		ManageData.doctorList.add(james);
		ManageData.doctorList.add(kobe);
		ManageData.doctorList.add(curry);
                ManageData.secretaryList.add(secretary);
		ManageData.appointmentsList.add(appointment);
		ManageData.prescriptionsList.add(prescription);
		ManageData.administratorList.add(admin);

	}
}
