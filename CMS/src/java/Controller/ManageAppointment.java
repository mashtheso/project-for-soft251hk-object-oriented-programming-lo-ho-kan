
package Controller;



import java.io.IOException;
import java.io.PrintWriter;

import javax.jws.Oneway;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import design.MessageServer;
import design.Observer;
import pojo.Appointment;
import pojo.Doctor;
import pojo.Patient;

/**
 *
 * @author LHK
 */
@WebServlet(name = "ManageAppointmentServlet",urlPatterns ="/ManageAppointmentServlet")
public class ManageAppointment extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DaoFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("patientID");
		String doctorID = request.getParameter("doctorID");
		String startTime = request.getParameter("startTime");	
		String endTime = request.getParameter("endTime");
		
		Appointment appointment = new Appointment(patientID,doctorID,startTime, endTime);
		
		ManageData.appointmentsList.add(appointment);
		
		MessageServer messageServer = new MessageServer();
		Observer patientObserver = null;
		Observer doctorObserver = null;
		for(Patient patient : ManageData.patientList) {
			if(patient.getId().equals(patientID)) {
				patientObserver = patient;
			}
		}
		for(Doctor doctor : ManageData.doctorList) {
			if(doctor.getId().equals(doctorID)) {
				doctorObserver = doctor;
			}
		}
		messageServer.registerObserber(patientObserver);
		messageServer.registerObserber(doctorObserver);
		messageServer.setMessageAndNotifyAll(appointment);

		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Manage Appointment Success ！';");
		out.println("alert(str);");
		out.println("window.location.href='SecretaryPage.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();
	}
}
