
package Controller;



import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import pojo.Patient;

/**
 *
 * @author LHK
 */
@WebServlet(name = "CreateAccountByPatientServlet", urlPatterns = "/CreateAccountByPatientServlet") 
public class CreateAccountByPatientServert extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DaoFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String patientID = request.getParameter("id");
		String patientPwd = request.getParameter("pwd");
		int patientAge = Integer.valueOf(request.getParameter("age"));
		String patientAddress = request.getParameter("address");
		String patientGender = request.getParameter("gender");
		
		ManageData.patientList.add(new Patient(patientID, patientPwd, patientAge, patientGender, patientAddress));
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str=' create patient account success ！! !';");
		out.println("alert(str);");
		out.println("window.location.href='PatientPage.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();		
	}
}
