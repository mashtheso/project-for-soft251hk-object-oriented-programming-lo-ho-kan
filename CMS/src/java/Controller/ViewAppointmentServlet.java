
package Controller;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.io.PrintWriter;
import pojo.Appointment;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LHK
 */
@WebServlet(name = "ViewAppointmentServlet", urlPatterns = "/ViewAppointmentServlet")
public class ViewAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataOperator dataOperator;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DaoFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String patientID = request.getParameter("id");
		System.out.println("View Appointment ID: " + patientID);
		
		List<Appointment> appointment = new ArrayList<>();
		for (Appointment app : ManageData.appointmentsList) {
			if(patientID.equals(app.getPatientid())) {
				appointment.add(app);
			}
                       
		}
		request.setAttribute("list", appointment);
		request.getRequestDispatcher("/patient/ViewAppointment.jsp").forward(request, response);
	}
}
