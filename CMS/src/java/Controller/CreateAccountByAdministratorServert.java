
package Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import pojo.Administrator;
import pojo.Doctor;
import pojo.Patient;
import pojo.Secretary;

/**
 *
 * @author LHK
 */
@WebServlet(name = "CreateAccountByAdministratorServlet", urlPatterns = "/CreateAccountByAdministratorServlet") // 注释
public class CreateAccountByAdministratorServert extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DaoFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String accountID = request.getParameter("id");
		String accountPwd = request.getParameter("pwd");
		String accountType = request.getParameter("UserSort");
		if(accountType.equals("Patient")) {
			ManageData.patientList.add(new Patient(accountID, accountPwd));
		}else if(accountType.equals("Doctor")) {
			ManageData.doctorList.add(new Doctor(accountID, accountPwd));
		}else if(accountType.equals("Administrator")) {
			ManageData.administratorList.add(new Administrator(accountID, accountPwd));
		}else if(accountType.equals("Secretary")) {
			ManageData.secretaryList.add(new Secretary(accountID, accountPwd));
		}
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str=' create account success ！! !';");
		out.println("alert(str);");
		out.println("window.location.href='AdministratorPage.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();	
	}
}
