package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import pojo.*;
 

/**
 *
 *
 * @author LHK
 */
@WebServlet(name = "DelectUserServlet", urlPatterns = "/DelectUserServlet")
public class DelectUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DaoFactory.getSingleDataOperator();
	}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
                String userID = request.getParameter("userID");
		String userPwd = request.getParameter("userPwd");
		String userType = request.getParameter("userType");
                

                  if(userType.equals("Patient")) {
			ManageData.patientList.add(new Patient(userID, userPwd));
		}else if(userType.equals("Doctor")) {
			ManageData.doctorList.add(new Doctor(userID, userPwd));
		}else if(userType.equals("Administrator")) {
			ManageData.administratorList.add(new Administrator(userID, userPwd));
		}else if(userType.equals("Secretary")) {
			ManageData.secretaryList.add(new Secretary(userID, userPwd));
		}
                
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str=' Remove User Success ！! !';");
		out.println("alert(str);"); 
		out.println("window.location.href='AdministratorPage.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();	
	}
}
