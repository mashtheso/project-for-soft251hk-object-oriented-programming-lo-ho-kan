
package Controller;



import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import data.DaoFactory;
import data.DataOperator;
import data.ManageData;

/**
 *
 * @author LHK
 */
@WebServlet(name = "LoginServlet",urlPatterns ="/LoginServlet")
public class LoginServlet extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
		private DataOperator dataOperator = null;
		  
    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
			dataOperator = DaoFactory.getSingleDataOperator();
		}

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			response.setContentType("text/html");
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			String type = request.getParameter("UserSort");	
			String username = request.getParameter("username");			
			String pwd = request.getParameter("pwd");
			System.out.print(type + ":" + username + ":" + pwd);
			
			new ManageData().init();
			
//			HttpSession session = request.getSession();// create Session
			if(dataOperator.vaildUser(type, username, pwd)) {
				 if(type.equals("Patient")) {
			   			request.getRequestDispatcher("/PatientPage.jsp").forward(request, response);
			   		 }else if(type.equals("Administrator")) {
			   			request.getRequestDispatcher("/AdministratorPage.jsp").forward(request, response);
			   		 }else if(type.equals("Secretary")) {
			   			request.getRequestDispatcher("/SecretaryPage.jsp").forward(request, response);
			   		 }else if(type.equals("Doctor")){
			   			request.getRequestDispatcher("/DoctorPage.jsp").forward(request, response);
					}
			}
			else {
				PrintWriter out = response.getWriter();
				out.println("<script language='javascript'>");
				out.println("var str='Input is incorrect! please input again!';");
				out.println("alert(str);");
				out.println("window.location.href='index.jsp';");
				out.println("</script>");
				out.flush();
				out.close();

			}
		}
}
