package design;
import java.util.ArrayList;
import java.util.List;

import pojo.Appointment;

/**
 *
 * @author LHK
 */
public class MessageServer implements Observerable {
	
	private List<Observer> observersList;
	private Appointment appointment;
	
    /**
     *
     */
    public MessageServer() {
		observersList = new ArrayList<>();
	}

@Override
	public void registerObserber(Observer observer) {
		observersList.add(observer);
	}

@Override
	public void removeObserver(Observer observer) {
		if(!observersList.isEmpty()) {
			observersList.remove(observer);
		}
	}

    /**
     *
     */
    public void notifyObserver() {
		for(Observer observer : observersList) {
			observer.update(appointment);
		}
	}

    /**
     *
     * @param appointment
     */
    public void setMessageAndNotifyAll(Appointment appointment) {
		this.appointment = appointment;
		notifyObserver();
	}

}
