
package design;

import pojo.Appointment;

/**
 *
 * @author LHK
 */
public interface Observer  {

    /**
     *
     * @param appointment
     */
    public void update(Appointment appointment);

}