
package design;

/**
 *
 * @author LHK
 */
public interface Observerable {

    /**
     *
     * @param observer
     */
    public void registerObserber(Observer observer);

    /**
     *
     * @param observer
     */
    public void removeObserver(Observer observer);
}
