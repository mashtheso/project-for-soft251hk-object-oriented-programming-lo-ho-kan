
package pojo;

/**
 *
 * @author LHK
 */
public class Administrator extends User {
	private String role;

    /**
     *
     * @param id
     * @param pwd
     */
    public Administrator(String id, String pwd) {
		super(id, pwd);
		role = "administrator";
	}

    /**
     *
     * @return
     */
    public String getRole() {
		return role;
	}
}
