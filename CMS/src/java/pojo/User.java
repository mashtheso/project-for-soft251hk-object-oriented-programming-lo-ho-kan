
package pojo;

import design.Observer;

/**
 *
 * @author LHK
 */
public class User implements Observer {
	private String id;
	private String pwd;

    /**
     *
     * @param id
     * @param pwd
     */
    public User(String id,String pwd) {
		this.id = id;
		this.pwd = pwd;
	}

    /**
     *
     * @return
     */
    public String getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(String id) {
		this.id = id;
	}

    /**
     *
     * @return
     */
    public String getPwd() {
		return pwd;
	}

    /**
     *
     * @param pwd
     */
    public void setPwd(String pwd) {
		this.pwd = pwd;
	}

    /**
     *
     * @param appointment
     */
    @Override
	public void update(Appointment appointment) {
		System.out.println("I get the Appointment: " + appointment);
	}
	
}