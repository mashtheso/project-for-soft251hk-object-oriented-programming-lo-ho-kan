
package pojo;

/**
 *
 * @author LHK
 */
public class Secretary extends User{
	
	private String role;

    /**
     *
     * @param id
     * @param pwd
     */
    public Secretary(String id, String pwd) {
		super(id, pwd);
		role = "secretary";
	}

    /**
     *
     * @return
     */
    public String getRole() {
		return role;
    }
}
