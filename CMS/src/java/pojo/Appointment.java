
package pojo;

/**
 *
 * @author LHK
 */
public class Appointment {
	
	private String patientid;
	private String doctorid;
	private String startTime;
	private String endTime;

    /**
     *
     * @param patientId
     * @param doctorId
     * @param startTime
     * @param endTime
     */
    public Appointment(String patientId,String doctorId ,String startTime, String endTime) {
		this.patientid= patientId;
		this.doctorid = doctorId;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
    /**
     *
     * @return
     */
    public String getPatientid() {
		return patientid;
	}

    /**
     *
     * @param patientid
     */
    public void setPatientid(String patientid) {
		this.patientid = patientid;
	}

    /**
     *
     * @return
     */
    public String getDoctorid() {
		return doctorid;
	}

    /**
     *
     * @param doctorid
     */
    public void setDoctorid(String doctorid) {
		this.doctorid = doctorid;
	}

    /**
     *
     * @return
     */
    public String getStartTime() {
		return startTime;
	}

    /**
     *
     * @param startTime
     */
    public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

    /**
     *
     * @return
     */
    public String getEndTime() {
		return endTime;
	}

    /**
     *
     * @param endTime
     */
    public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}
