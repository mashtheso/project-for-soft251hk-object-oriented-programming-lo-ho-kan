
package pojo;

/**
 *
 * @author LHK
 */
public class Doctor extends User{
	
	private String role;
	private String ratings;
	private Appointment appointment;
	private String address;

    /**
     *
     * @param id
     * @param pwd
     */
    public Doctor(String id, String pwd) {
		super(id, pwd);
		role = "doctor";
	}

    /**
     *
     * @return
     */
    public String getRole() {
		return role;
	}

    /**
     *
     * @return
     */
    public String getRatings() {
		return ratings;
	}

    /**
     *
     * @param ratings
     */
    public void setRatings(String ratings) {
		this.ratings = ratings;
	}

    /**
     *
     * @return
     */
    public Appointment getAppointment() {
	return appointment;
	}

    /**
     *
     * @param appointment
     */
    public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

    /**
     *
     * @return
     */
    public String getAddress() {
		return address;
	}

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
		this.address = address;
	}

    @Override
	public void update(Appointment appointment) {
		if(getId().equals(appointment.getDoctorid()))
			this.appointment = appointment;
	}
}
