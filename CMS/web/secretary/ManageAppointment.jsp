
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Manage Appointment</title>
</head>
<body bgcolor="silver">
    <div  style=" position:right; border:thick ;background-color:#9F9"> <div align="center"><b>Clinic Management System(CMS)</b></div>
     <div  align="right"> <button onclick="window.location.href = '../index.jsp';">Logout</button></div><hr>
	<form name="manage_appointment_from"
		action="../ManageAppointmentServlet" method="post" target="_self">
		<table align="center">
			<tr>
				<td align="center" colspan="2">
					<h3>Appointment for Patient</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td width="240px">Patient ID：</td>
				<td><input type="text" name="patientID" /></td>
			</tr>
			<tr>
				<td width="240px">Doctor ID：</td>
				<td><input type="text" name="doctorID" /></td>
			</tr>
			<tr>
				<td width="240px">Start Time：</td>
				<td><input type="text" name="startTime" /></td>
			</tr>
			<tr>
				<td width="240px">End Time：</td>
				<td><input type="text" name="endTime" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Manage"></td>
			</tr>
		</table>
	</form>
</body>
</html>
