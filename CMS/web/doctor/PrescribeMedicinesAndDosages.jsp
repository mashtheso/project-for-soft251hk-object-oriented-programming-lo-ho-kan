
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Prescribe Medicines</title>
</head>
<body bgcolor="silver">
    <div  style=" position:right; border:thick ;background-color:#9F9"> <div align="center"><b>Clinic Management System(CMS)</b></div>
     <div  align="right"> <button onclick="window.location.href = '../index.jsp';">Logout</button></div><hr>
	<form name="prescribe_medicines_from"
		action="../PrescribeMedicinesServlet" method="post" target="_self">
		<table align="center">
			<tr>
				<td align="center" colspan="2">
					<h3>Prescribe for Patient</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td width="240px">Patient ID：</td>
				<td><input type="text" name="patientID" /></td>
			</tr>
			<tr>
				<td width="240px">Medicine：</td>
				<td><input type="text" name="medicine" /></td>
			</tr>
			<tr>
				<td width="240px">Quantity：</td>
				<td><input type="text" name="quantity" /></td>
			</tr>
			<tr>
				<td width="240px">Dosage：</td>
				<td><input type="text" name="dosage" /></td>
			</tr>
			<tr>
				<td width="240px">Doctor ID：</td>
				<td><input type="text" name="doctorID" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Prescribe"></td>
			</tr>
		</table>
	</form>
</body>
</html>
