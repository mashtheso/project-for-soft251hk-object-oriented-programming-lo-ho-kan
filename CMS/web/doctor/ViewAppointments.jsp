
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Patient Appointment</title>
</head>
<body bgcolor="silver">
    <div  style=" position:right; border:thick ;background-color:#9F9"> <div align="center"><b>Clinic Management System(CMS)</b></div>
     <div  align="right"> <button onclick="window.location.href = '../index.jsp';">Logout</button></div><hr>
	<form name="doctor_appointment_from" action="../ViewDoctorAppointmentServlet"
		method="post">
		<table align="center">
			<tr>
				<td align="center" colspan="4">
					<h3>View Appointments</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td width="160px">Doctor's Unique ID：</td>
				<td><input type="text" name="id" /></td>
			</tr>
			<tr align="center" bgcolor="#e1ffc1">
				<td><b>patient ID</b></td>
				<td><b>doctor ID</b></td>
				<td><b>start Time</b></td>
				<td><b>end Time</b></td>
			</tr>
			<c:forEach items="${requestScope.list}" var="appointment">
				<tr align="center" bgcolor="white">
					<td>${appointment.getPatientid()}</td>
					<td>${appointment.getDoctorid()}</td>
					<td>${appointment.getStartTime()}</td>
					<td>${appointment.getEndTime()}</td>
				</tr>
			</c:forEach>
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="View"></td>
			</tr>
		</table>
	</form>
</body>
</html>
